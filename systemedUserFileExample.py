sudo apt-get install libpam-systemd -y

# maybe needed to exit and login again to activate XDG_RUNTIME_DIR environment 
# variable and "/lib/systemd/systemd --user"

mkdir ~/bin
cat << EOF > ~/bin/mytest.py
#!/usr/bin/env python3

import datetime
import os
import sys 
import time

INTERVAL = 3

message = sys.argv[1] if len(sys.argv) >= 2 else "default message"

f = open('/tmp/test.log','w')
while True:
    f.write('OK: ' + str(datetime.datetime.now()) + os.linesep)
    f.write('sys.argv[1]:' + sys.argv[1] + os.linesep)
    f.flush()
    time.sleep(INTERVAL)
EOF

chmod a+x ~/bin/mytest.py
mkdir -p ~/.config/systemd/user

# equivalent to: cd ~/.config/systemd/user
cd $_

mkdir mytest.target.wants

cat << EOF > mytest.target.wants/mytest.service
[Unit]
Description=test service
After=network-online.target

[Service]
Type=simple
ExecStart=/bin/sh -c '${HOME}/bin/mytest.py my_regular_log'
EOF

cat << EOF >> mytest.target
[Unit]
Description=mytest target

[Install]
Alias=default.target
EOF

# Then run as the standard user:
systemctl --user daemon-reload
systemctl --user enable mytest.target

'''
Compléments:

# commande complémentaire que j'ai executé pour autoriser les services 
# de type utilisateur standard à se lancer tout seul au démarrage.
sudo -i
loginctl enable-linger pi

# et en cas de problème, voici les commandes pour savoir ce qui a cloché.
systemctl --user status
systemctl --user status startsshremote.service
'''