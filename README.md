# Description

![Alt-Text](https://cdn.nicolapps.ch/images/uploads/1545206329.jpeg)

R�alisation d'**un projet portable** (sur raspberry pi) qui a pour but de r�cup�rer des **images** (photos) de la Terre en direct gr�ce � une **antenne v-dip�le** (ou autre selon prix/comptabilit�). L�ordinateur(raspberry pi) sera portable gr�ce � une alimentation externe (panneau solaire).
## Utilisation possible du projet

Cr�ation d�une mini station m�t�o autonome qui permet d�afficher des images m�t�orologique dans la zone ou elle se situe. Les photos peuvent �tre visionn�es par des utilisateurs en temps r�els (actualisation des images chaque heure environ).

## Mat�riel utilis�
* Cl� SDR Nooelec R820T2 + TCXO + SMA
* Raspberry PI 3B+
* Antenne V-Dipole ([tuto construction](https://www.youtube.com/watch?v=mGPpEhBBoAs))
* Cable SMA

### Optionel
* Filtre FM
* Tr�pied


## Logiciels utilis�s
* Gpredict
* Wxtoimg
* SDR#
* SDR-Console
* Predict

## Faire sa premi�re capture (manuel)
[Tuto](https://www.youtube.com/watch?v=1G9kDGmQxtY) r�aliser par TysonPower


## Configuration du syst�me d'automatisation
* Tutoriel pour l'automatisation des captures satellites : [instructables.com](https://www.instructables.com/id/Raspberry-Pi-NOAA-Weather-Satellite-Receiver/)
* Tutoriel pour la configuration du reverse ssh : [geekfault.org](https://geekfault.org/2011/02/19/reverse-ssh-acceder-a-un-serveur-derriere-un-natfirewall/)
* Exemple de script pour l'ancer le rerverse ssh au d�marage grace a systemd : [systemedUserFileExample.py](/systemedUserFileExample.py)

## Credits
Remerciementt 
[TysonPower](https://twitter.com/Tysonpower)<br/>
Ecole
[CFPT Informatique - Gen�ve](https://edu.ge.ch/site/cfpt/)

## License
[MIT](https://choosealicense.com/licenses/mit/)